package com.assignment2.assignment_2_databases;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment2DatabasesApplication {

	public static void main(String[] args) {
		SpringApplication.run(Assignment2DatabasesApplication.class, args);
	}

}
