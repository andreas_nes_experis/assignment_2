package com.example.assignment2.model;

import java.util.List;

public record CustomerGenre(
        int customerId,
        List<String> genre
) {
}
