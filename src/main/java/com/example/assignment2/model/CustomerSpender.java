package com.example.assignment2.model;

public record CustomerSpender(
        int customerId,
        String firstName,
        String lastName,
        double invoiceTotal
) {
}
