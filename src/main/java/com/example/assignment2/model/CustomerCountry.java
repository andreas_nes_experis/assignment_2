package com.example.assignment2.model;

public record CustomerCountry(
        String country,
        int numberOfCustomers
) {
}
