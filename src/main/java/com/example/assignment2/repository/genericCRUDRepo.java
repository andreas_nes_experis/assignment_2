package com.example.assignment2.repository;

import java.util.List;

public interface genericCRUDRepo<T, U> {
    List<T> findAll(boolean print);
    T findById(int id, boolean print);
    void insert(T object);
    void update(int id, String columnName, String newValue);
}
