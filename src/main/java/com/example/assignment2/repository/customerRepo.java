package com.example.assignment2.repository;

import java.util.List;

public interface customerRepo extends genericCRUDRepo<com.example.assignment2.model.Customer, Integer> {
    com.example.assignment2.model.Customer findByName(String name, boolean print);
    List<com.example.assignment2.model.Customer> findPage(int limit, int offset, boolean print);
    com.example.assignment2.model.CustomerSpender findBiggestSpender(boolean print);
    com.example.assignment2.model.CustomerCountry getCountryWithMostCustomers(boolean print);
    com.example.assignment2.model.CustomerGenre getMostPopularGenreFromCustomer(int id, boolean print);
}
