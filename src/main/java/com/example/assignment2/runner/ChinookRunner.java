package com.example.assignment2.runner;

import com.example.assignment2.access.ChinookDAO;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class ChinookRunner implements ApplicationRunner {

    private final ChinookDAO chinookDAO;

    public ChinookRunner(ChinookDAO chinookDAO) {
        this.chinookDAO = chinookDAO;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {


    }
}
