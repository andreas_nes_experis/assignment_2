package com.example.assignment2;

import com.example.assignment2.access.ChinookDAO;
import com.example.assignment2.model.Customer;
import com.example.assignment2.model.CustomerCountry;
import com.example.assignment2.model.CustomerSpender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class Assignment2Application implements ApplicationRunner {
    @Autowired  // Didn't work without Autowired
    ChinookDAO chinookDAO;

    public static void main(String[] args) {
        SpringApplication.run(Assignment2Application.class, args);

    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        chinookDAO.testConnection();
        // 1. Test displaying all customers
        List<Customer> customers = chinookDAO.findAll(false);

        // 2. Test displaying a specific customer by ID
        Customer customer1 = chinookDAO.findById(15, false); // By ID
        // 3. Test displaying a specific customer by name
        Customer customer2 = chinookDAO.findByName("Frank", false); // By name

        // 4. Test displaying a page of customers
        List<Customer> pageOfCustomers = chinookDAO.findPage(4, 5, false);

        // 5. Add new customer to database
        Customer newCustomer = new Customer(
                60,
                "Frode",
                "Frodesen",
                "Frodeland",
                "0001",
                "+0 00 000 01",
                "frode@frode.fl"
        );
        //chinookDAO.addCustomer(newCustomer);
        //Customer frode = chinookDAO.getSpecificCustomer(60, false);

        // 6. Test displaying the updated customer
        chinookDAO.update(9, "last_name", "Pettersen");
        Customer updatedCustomer = chinookDAO.findById(9, false);

        // 7. Get country with most customers
        CustomerCountry customerCountry = chinookDAO.getCountryWithMostCustomers(false);

        // 8. Get customer who is the highest spender
        CustomerSpender customerSpender = chinookDAO.findBiggestSpender(false);

        // 9. Get most popular genre from specific customer
        chinookDAO.getMostPopularGenreFromCustomer(10, true);
    }
}
