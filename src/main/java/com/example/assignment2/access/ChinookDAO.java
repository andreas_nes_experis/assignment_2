package com.example.assignment2.access;

import com.example.assignment2.model.Customer;
import com.example.assignment2.model.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.example.assignment2.repository.customerRepo;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@Component
public class ChinookDAO implements customerRepo {
    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;

    public ChinookDAO() {}

    /**
     * Method for testing connection to database
     */
    public void testConnection(){
        try(Connection conn = DriverManager.getConnection(url, username, password)){
            System.out.println("Connected!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Get all customers from the customer table and save it to a list of Customer records
     * @param print boolean parameter to enable printing each customer to the console
     * @return list of customer records
     */
    public List<Customer> findAll(boolean print) {
        String sql = "Select * from customer";
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result: for each Customer...
            while (result.next()){
                // Build Customer record
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                // Print if requested in parameters
                if (print) {
                    System.out.println(customer.toString());
                }
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    /**
     * Get a specific customer by customer id as a Customer record
     * @param id customer id of customer we want
     * @param print boolean parameter to enable printing of the customer to the console
     * @return Customer record
     */
    public Customer findById(int id, boolean print) {
        String sql = "SELECT * FROM customer WHERE customer_id = ?";
        Customer customer = null;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            // Execute statement
            ResultSet result = statement.executeQuery();
            result.next();
            // Build Customer record
            customer = new Customer(
                    result.getInt("customer_id"),
                    result.getString("first_name"),
                    result.getString("last_name"),
                    result.getString("country"),
                    result.getString("postal_code"),
                    result.getString("phone"),
                    result.getString("email")
            );
            // Print if requested in parameters
            if (print) {
                System.out.println(customer.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    /**
     * Get a specific customer by customer name as a Customer record
     * @param name customer name of customer we want
     * @param print boolean parameter to enable printing of the customer to the console
     * @return Customer record
     */
    public Customer findByName(String name, boolean print) {
        String sql = "SELECT * FROM customer WHERE first_name = ?";
        Customer customer = null;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, name);
            // Execute statement
            ResultSet result = statement.executeQuery();
            result.next();
            // Build Customer record
            customer = new Customer(
                    result.getInt("customer_id"),
                    result.getString("first_name"),
                    result.getString("last_name"),
                    result.getString("country"),
                    result.getString("postal_code"),
                    result.getString("phone"),
                    result.getString("email")
            );
            // Print if requested in parameters
            if (print) {
                System.out.println(customer.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }
    /**
     * Get subset of customers from table based on offset and limit
     * @param limit How many customers to return
     * @param offset Starting point of customer subset
     * @param print boolean for printing customers to console
     * @return list of customer subset
     */
    public List<Customer> findPage(int limit, int offset, boolean print) {
        String sql = "Select * from customer LIMIT ? OFFSET ?";
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, limit);
            statement.setInt(2, offset);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result: for each Customer...
            while (result.next()){
                // Build Customer record
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                // Print if requested in parameters
                if (print) {
                    System.out.println(customer.toString());
                }
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    /**
     * Method for adding customer to customer table.
     * If provided id already exists, the method auto increments it to the existing ids
     * @param customer customer record to add to the database
     */
    public void insert(Customer customer) {
        String sql = "INSERT INTO customer (customer_id, first_name, last_name, country, postal_code, phone, email) VALUES (?, ?, ?, ?, ?, ?, ?)";
        // Get the largest id value in the customer database
        int maxId = getMaxCustomerId();
        // If our customer id is within the existing ids, we set it to current largest id + 1
        if (customer.id() <= maxId){
            customer = new Customer(
                    maxId + 1,
                    customer.firstName(),
                    customer.lastName(),
                    customer.country(),
                    customer.postalCode(),
                    customer.phone(),
                    customer.email()
            );
        }
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customer.id());
            statement.setString(2, customer.firstName());
            statement.setString(3, customer.lastName());
            statement.setString(4, customer.country());
            statement.setString(5, customer.postalCode());
            statement.setString(6, customer.phone());
            statement.setString(7, customer.email());
            // Execute statement
            int affectedRows = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Helper method for insert to manually auto increment id
     * @return maximum id of existing customers
     */
    private int getMaxCustomerId() {
        int id = 0;
        String sql = "SELECT MAX(customer_id) AS id FROM customer";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            result.next();
            int customerId = result.getInt("id");
            return customerId;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    /**
     * Method for updating a value in an existing customer
     * @param id id of customer we want to update
     * @param columnName name of column to update
     * @param newValue new updated value to insert
     */
    public void update(int id, String columnName, String newValue) {
        String sql = "UPDATE customer SET "+ columnName + " = ? WHERE customer_id = ?";
        Customer customer = null;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, newValue);
            statement.setInt(2, id);
            // Execute statement
            int result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method for getting the most spending customer as a CustomerSpender record
     * @param print boolean for printing the CustomerSpender record
     * @return CustomerSpender record
     */
    public CustomerSpender findBiggestSpender(boolean print){
        String sql = "SELECT customer_id, total FROM invoice WHERE total = (SELECT MAX(total) FROM invoice)";
        CustomerSpender customerSpender = null;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            result.next();
            int customerId = result.getInt("customer_id");
            double invoiceTotal = result.getDouble("total");
            Customer customer = findById(customerId, false);
            customerSpender = new CustomerSpender(
                    customerId,
                    customer.firstName(),
                    customer.lastName(),
                    invoiceTotal
            );
            if (print) {
                System.out.println(customerSpender.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerSpender;
    }

    /**
     * Method for getting the country with the most customers as a CustomerCountry record
     * @param print boolean for printing the CustomerCountry record
     * @return CustomerCountry record
     */
    public CustomerCountry getCountryWithMostCustomers(boolean print) {
        String sql = "SELECT country,COUNT(country) AS COUNT FROM customer GROUP BY country ORDER BY COUNT DESC LIMIT 1";
        String countryWithMostCustomers = null;
        CustomerCountry customerCountry = null;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute query
            ResultSet result = statement.executeQuery();
            // Handle result
            result.next();
            customerCountry = new CustomerCountry(
                    result.getString("country"),
                    result.getInt("COUNT")
            );
            if (print) {
                System.out.println(customerCountry.toString());
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return customerCountry;
    }

    /**
     * Method for getting the most popular genres of the given customer by id as a CustomerGenre record
     * @param id customer id of target customer
     * @param print boolean for printing the CustomerGenre record
     * @return CustomerGenre record
     */
    public CustomerGenre getMostPopularGenreFromCustomer(int id, boolean print) {
        int invoiceId = getInvoiceIdFromCostumerId(id);
        List<Integer> trackIds = getTrackIdsFromInvoiceId(invoiceId);
        List<Integer> genreIds = getGenreIdsFromTrackIds(trackIds);
        List<Integer> favouriteGenreIds = getMostFrequentGenres(genreIds);
        List<String> favouriteGenreNames = getGenreNamesFromGenreIds(favouriteGenreIds);
        CustomerGenre customerGenre = new CustomerGenre(
                id,
                favouriteGenreNames
        );
        if (print) {
            System.out.println(customerGenre.toString());
        }
        return customerGenre;
    }

    /**
     * getMostPopularGenreFromCustomer helper method
     * @param customerId customer id of target customer
     * @return invoice id of target customer
     */
    private int getInvoiceIdFromCostumerId(int customerId) {
        String sql = "SELECT invoice_id FROM invoice WHERE customer_id = ?";
        int invoiceId = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Prepare statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customerId);
            // Write statement
            ResultSet result = statement.executeQuery();
            result.next();
            invoiceId = result.getInt("invoice_id");
            return invoiceId;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return invoiceId;
    }

    /**
     * getMostPopularGenreFromCustomer helper method
     * @param invoiceId invoice id of target customer
     * @return list of track ids for target customer
     */
    private List<Integer> getTrackIdsFromInvoiceId(int invoiceId) {
        String sql = "SELECT * FROM invoice_line WHERE invoice_id = ?";
        List<Integer> trackIds = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Prepare statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, invoiceId);
            // Write statement
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                int trackId = result.getInt("track_id");
                trackIds.add(trackId);
            }
            return trackIds;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return trackIds;
    }

    /**
     * getMostPopularGenreFromCustomer helper method
     * @param trackIds list of track ids for target customer
     * @return list of genre id for each track in track id list
     */
    private List<Integer> getGenreIdsFromTrackIds(List<Integer> trackIds) {
        List<Integer> genreIds = new ArrayList<>();
        for (int trackId : trackIds) {
            genreIds.add(getGenreIdFromTrackId(trackId));
        }
        return genreIds;
    }

    /**
     * getGenreIdsFromTrackIds helper method
     * @param trackId current track id
     * @return genre id of current track
     */
    private int getGenreIdFromTrackId(int trackId) {
        String sql = "SELECT genre_id FROM track WHERE track_id = ?";
        int genreId = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Prepare statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, trackId);
            // Write statement
            ResultSet result = statement.executeQuery();
            result.next();
            genreId = result.getInt("genre_id");
            return genreId;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return genreId;
    }

    /**
     * getMostPopularGenreFromCustomer helper method
     * @param genreIds genre id of each track from target customer
     * @return list of genre ids for most popular genres
     */
    private List<Integer> getMostFrequentGenres(List<Integer> genreIds) {
        HashMap<Integer, Integer> genreIdMap = new HashMap<>();
        for (int i : genreIds) {
            if (genreIdMap.containsKey(i)) {
                int n = genreIdMap.get(i) + 1;
                genreIdMap.put(i, n);
            } else {
                genreIdMap.put(i, 1);
            }
        }
        int maxFreq = Collections.max(genreIdMap.values());
        List<Integer> favouriteGenreIds = new ArrayList<>();
        for (int key : genreIdMap.keySet()) {
            if (genreIdMap.get(key) == maxFreq) {
                favouriteGenreIds.add(key);
            }
        }
        return favouriteGenreIds;
    }

    /**
     * getMostPopularGenreFromCustomer
     * @param genreIds genre id of most popular genres
     * @return list of names of most popular genres
     */
    private List<String> getGenreNamesFromGenreIds(List<Integer> genreIds) {
        List<String> names = new ArrayList<>();
        for (int genreId : genreIds) {
            names.add(getGenreNameFromGenreId(genreId));
        }
        return names;
    }

    /**
     * getGenreNamesFromGenreIds helper method
     * @param genreId genre id of current track
     * @return genre name of current track
     */
    private String getGenreNameFromGenreId(int genreId) {
        String sql = "SELECT name FROM genre WHERE genre_id = ?";
        String name = null;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Prepare statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, genreId);
            // Write statement
            ResultSet result = statement.executeQuery();
            result.next();
            name = result.getString("name");
            return name;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return name;
    }
}
