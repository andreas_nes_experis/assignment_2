/* Link assistant to superhero. Assistant own the relationship
since one superhero can have many assistants */
ALTER TABLE assistant ADD COLUMN superhero_id int REFERENCES superhero;