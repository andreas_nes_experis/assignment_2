
DROP TABLE IF EXISTS superhero_power;
DROP TABLE IF EXISTS assistant;
DROP TABLE IF EXISTS power;
DROP TABLE IF EXISTS superhero;

CREATE TABLE superhero(
	id serial PRIMARY KEY,
	name varchar(50) NOT NULL,
	alias varchar(50),
	origin varchar(50)
);

CREATE TABLE assistant (
	id serial PRIMARY KEY,
	name varchar(50) NOT NULL
);

CREATE TABLE power (
	id serial PRIMARY KEY,
	name varchar(50) NOT NULL,
	description text
);