/* Add powers to power table */
INSERT INTO power (name) VALUES ('Heat vision');
INSERT INTO power (name) VALUES ('Web shot');
INSERT INTO power (name) VALUES ('Punch');
INSERT INTO power (name) VALUES ('Flight');

/* Associate powers with superheroes */
INSERT INTO superhero_power (superhero_id, power_id) VALUES (2, 1);
INSERT INTO superhero_power (superhero_id, power_id) VALUES (3, 2);
INSERT INTO superhero_power (superhero_id, power_id) VALUES (2, 4);
INSERT INTO superhero_power (superhero_id, power_id) VALUES (1, 3);
INSERT INTO superhero_power (superhero_id, power_id) VALUES (2, 3);
INSERT INTO superhero_power (superhero_id, power_id) VALUES (3, 3);