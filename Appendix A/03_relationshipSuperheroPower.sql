DROP TABLE IF EXISTS superhero_power;

/* Linking table between superhero and power */
CREATE TABLE superhero_power (
	superhero_id int REFERENCES superhero,
	power_id int REFERENCES power,
	PRIMARY KEY (superhero_id, power_id)
);