INSERT INTO superhero (name, alias, origin) VALUES ('Bruce Wayne', 'Batman', 'Gotham');
INSERT INTO superhero (name, alias, origin) VALUES ('Clark Kent', 'Superman', 'Krypton');
INSERT INTO superhero (name, alias, origin) VALUES ('Peter Parker', 'Spider-Man', 'New York City');